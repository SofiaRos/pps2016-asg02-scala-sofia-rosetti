package characters

import java.awt.Image

import game.PlatformImpl
import objects.{GameObjectImpl, Piece}
import utils.{Res, Utils}

/**
  * Created by Sofia Rosetti on 11/04/2017.
  */

object Mario {
  private val MARIO_OFFSET_Y_INITIAL = 243
  private val FLOOR_OFFSET_Y_INITIAL = 293
  private val WIDTH = 28
  private val HEIGHT = 50
  private val JUMPING_LIMIT = 42
}

class Mario(_x: Int, _y: Int) extends BasicCharacter(_x, _y, Mario.WIDTH, Mario.HEIGHT){

  private var marioImage: Image = Utils.getImage(Res.IMG_MARIO_DEFAULT)
  private var jumping: Boolean = false
  private var jumpingExtent: Int = 0

  def isJumping: Boolean = jumping

  def setJumping(j: Boolean): Unit = jumping = j

  def doJump(): Image = {
    var str: String = null
    jumpingExtent = jumpingExtent + 1

    if (jumpingExtent < Mario.JUMPING_LIMIT) {
      if (y > PlatformImpl.getPlatform.getHeightLimit) {
        y = y - 4
      } else {
        jumpingExtent = Mario.JUMPING_LIMIT
      }
      if (isToRight) {
        str = Res.IMG_MARIO_SUPER_DX
      } else {
        str = Res.IMG_MARIO_SUPER_SX
      }
    } else if ((y + height) < PlatformImpl.getPlatform.getFloorOffsetY) {
      y = y + 1
      if (isToRight) {
        str = Res.IMG_MARIO_SUPER_DX
      } else {
        str = Res.IMG_MARIO_SUPER_SX
      }
    } else {
      if (isToRight) {
        str = Res.IMG_MARIO_ACTIVE_DX
      } else {
        str = Res.IMG_MARIO_ACTIVE_SX
      }
      jumping = false
      jumpingExtent = 0
    }
    Utils.getImage(str)
  }

  def contactPiece(piece: Piece): Boolean = {
    hitBack(piece) || hitAbove(piece) || hitAhead(piece) || hitBelow(piece)
  }

  def contact(obj: GameObjectImpl): Unit = {
    if ((hitAhead(obj) && isToRight) || (hitBack(obj) && !isToRight)) {
      PlatformImpl.getPlatform.setMov(0)
      isMoving = false
    }
    if (hitBelow(obj) && isJumping) {
      PlatformImpl.getPlatform.setFloorOffsetY(obj.y)
    } else if (!hitBelow(obj)) {
      PlatformImpl.getPlatform.setFloorOffsetY(Mario.FLOOR_OFFSET_Y_INITIAL)
      if (!jumping){
        y = Mario.MARIO_OFFSET_Y_INITIAL
      }
      if(hitAbove(obj)) {
        PlatformImpl.getPlatform.setHeightLimit(obj.y + obj.height)
      } else if (!hitAbove(obj) && !jumping){
        PlatformImpl.getPlatform.setHeightLimit(0)
      }
    }
  }

  def contact(bc: BasicCharacter): Unit = {
    if (hitAhead(bc) || hitBack(bc)) {
      if (bc.isAlive) {
        isMoving = false
        isAlive = false
      } else {
        isAlive = true
      }
    } else if (hitBelow(bc)) {
      bc.isMoving = false
      bc.isAlive = false
    }
  }

}
