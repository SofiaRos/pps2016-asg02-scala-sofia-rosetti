package characters

import java.awt.Image

import objects.GameObjectImpl
import utils.{Res, Utils}

/**
  * Created by Sofia Rosetti on 11/04/2017.
  */

object Turtle {
  private val WIDTH = 43
  private val HEIGHT = 50
  private val PAUSE = 15
}
class Turtle(_x: Int, _y: Int) extends BasicCharacter(_x, _y, Turtle.WIDTH, Turtle.HEIGHT) with Runnable{

  private var turtleImage: Image = Utils.getImage(Res.IMG_TURTLE_IDLE)
  private var dxTurtle: Int = 1

  isToRight = true
  isMoving = true

  var chronoTurtle: Thread = new Thread(this)
  chronoTurtle start()

  def getDxTurtle: Int = dxTurtle

  def setDxTurtle(i: Int): Unit = dxTurtle = i

  override def move(): Unit = {
    if (isToRight) {
      setDxTurtle(1)
    } else {
      setDxTurtle(-1)
    }
    x_=(x + getDxTurtle)
  }

  override def run(): Unit = {
    while (true) {
      if (isAlive){
        move()
        Thread.sleep(Turtle.PAUSE)
      }
    }
  }

  def deadImage: Image = Utils.getImage(Res.IMG_TURTLE_DEAD)

  def contact(obj: GameObjectImpl): Unit = {
    if (hitAhead(obj) && isToRight){
      isToRight = false
      setDxTurtle(-1)
    } else {
      if (hitBack(obj) && !isToRight) {
        isToRight = true
        setDxTurtle(1)
      }
    }
  }

  def contact(bc: BasicCharacter): Unit = {
    if (hitAhead(bc) && !isToRight) {
      isToRight = true
      setDxTurtle(1)
    }
  }

}
