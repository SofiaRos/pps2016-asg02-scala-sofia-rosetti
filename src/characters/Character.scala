package characters

import java.awt.Image

import game.PlatformImpl
import objects.GameObjectImpl
import utils.{Res, Utils}

/**
  * Created by Sofia Rosetti on 09/03/2017.
  */

trait Character {

  def x: Int

  def y: Int

  def width: Int

  def height: Int

  def getCounter: Int

  def isAlive: Boolean

  def isMoving: Boolean

  def isToRight: Boolean

  def isAlive_=(aliveIn: Boolean): Unit

  def x_=(x: Int): Unit

  def y_=(y: Int): Unit

  def isMoving_=(moving: Boolean): Unit

  def isToRight_=(toRightIn: Boolean): Unit

  def setCounter(counter: Int): Unit

  def walk(name: String, frequency: Int): Image

  def move(): Unit

  def hitAhead(og: GameObjectImpl): Boolean

  def hitBelow(pers: BasicCharacter): Boolean

  def isNearby(pers: BasicCharacter): Boolean

  def isNearby(obj: GameObjectImpl): Boolean
}

object BasicCharacter {
  private val PROXIMITY_MARGIN: Int = 10
  private val HIT_MARGIN: Int = 5
}

class BasicCharacter(var _x: Int, var _y: Int, var _w: Int, var _h: Int) extends Character {

  private var moving: Boolean = false
  private var toRight: Boolean = false
  private var counter: Int = 0
  private var alive: Boolean = false

  this.counter = 0
  this.moving = false
  this.toRight = true
  this.alive = true

  override def x: Int = _x

  override def y: Int = _y

  override def width: Int = _w

  override def height: Int = _h

  override def getCounter: Int = counter

  override def isAlive: Boolean = alive

  override def isMoving: Boolean = moving

  override def isToRight: Boolean = toRight



  override def isAlive_=(aliveIn: Boolean) : Unit = alive = aliveIn

  override def x_=(x: Int): Unit = _x = x

  override def y_=(y: Int): Unit = _y = y

  override def isMoving_=(moving: Boolean) {
    this.moving = moving
  }

  override def isToRight_=(toRightIn: Boolean) : Unit = toRight = toRightIn

  override def setCounter(counter: Int) {
    this.counter = counter
  }

  override def walk(name: String, frequency: Int): Image = {
    val str: String = Res.IMG_BASE + name + (if (!this.moving || {
      this.counter += 1; this.counter
    } % frequency == 0) Res.IMGP_STATUS_ACTIVE
    else Res.IMGP_STATUS_NORMAL) + (if (this.toRight) Res.IMGP_DIRECTION_DX
    else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT
    Utils.getImage(str)
  }

  override def move() {
    if (PlatformImpl.getPlatform.getXPos >= 0) {
      this._x = this._x - PlatformImpl.getPlatform.getMov
    }
  }

  override def hitAhead(og: GameObjectImpl): Boolean = {
    !(this._x + this._w < og.x || this._x + this._w > og.x + characters.BasicCharacter.HIT_MARGIN ||
      this._y + this._h <= og.y || this._y >= og.y + og.height)
  }

  protected def hitBack(og: GameObjectImpl): Boolean = {
    !(this._x > og.x + og.width || this._x + this._w < og.x + og.width - characters.BasicCharacter.HIT_MARGIN ||
      this._y + this._h <= og.y || this._y >= og.y + og.height)
  }

  protected def hitBelow(og: GameObjectImpl): Boolean = {
    !(this._x + this._w < og.x + characters.BasicCharacter.HIT_MARGIN || this._x > og.x + og.width - characters.BasicCharacter.HIT_MARGIN ||
      this._y + this._h < og.y || this._y + this._h > og.y + 5)
  }

  protected def hitAbove(og: GameObjectImpl): Boolean = {
    !(this._x + this._w < og.x + characters.BasicCharacter.HIT_MARGIN || this._x > og.x + og.width - characters.BasicCharacter.HIT_MARGIN ||
      this._y < og.y + og.height || this._y > og.y + og.height + characters.BasicCharacter.HIT_MARGIN)
  }

  protected def hitAhead(pers: BasicCharacter): Boolean = {
    if (this.isToRight) {
      !(this._x + this._w < pers.x || this._x + this._w > pers.x + characters.BasicCharacter.HIT_MARGIN ||
        this._y + this._h <= pers.y || this._y >= pers.y + pers.height)
    }
    else {
      false
    }
  }

  protected def hitBack(pers: BasicCharacter): Boolean = {
    !(this._x > pers.x + pers.width || this._x + this._w < pers.x + pers.width - characters.BasicCharacter.HIT_MARGIN ||
      this._y + this._h <= pers.y || this._y >= pers.y + pers.height)
  }

  override def hitBelow(pers: BasicCharacter): Boolean = {
    !(this._x + this._w < pers.x || this._x > pers.x + pers.width || this._y + this._h < pers.y ||
      this._y + this._h > pers.y)
  }

  override def isNearby(pers: BasicCharacter): Boolean = {
    (this._x > pers.x - characters.BasicCharacter.PROXIMITY_MARGIN && this._x < pers.x + pers.width + characters.BasicCharacter.PROXIMITY_MARGIN) ||
      (this._x + this._w > pers.x - characters.BasicCharacter.PROXIMITY_MARGIN && this._x + this._w < pers.x + pers.width + characters.BasicCharacter.PROXIMITY_MARGIN)
  }

  override def isNearby(obj: GameObjectImpl): Boolean = {
    (this._x > obj.x - characters.BasicCharacter.PROXIMITY_MARGIN && this._x < obj.x + obj.width + characters.BasicCharacter.PROXIMITY_MARGIN) ||
      (this.x + this._w > obj.x - characters.BasicCharacter.PROXIMITY_MARGIN && this._x + this._w < obj.x + obj.width + characters.BasicCharacter.PROXIMITY_MARGIN)
  }
}