package characters

import java.awt.Image

import objects.GameObjectImpl
import utils.{Res, Utils}

/**
  * Created by Sofia Rosetti on 12/04/2017.
  */

object Plant {
  private val WIDTH = 43
  private val HEIGHT = 50
  private val PAUSE = 15
}

class Plant (_x: Int, _y: Int) extends BasicCharacter(_x, _y, Plant.WIDTH, Plant.HEIGHT) with Runnable {

  private var plantImage: Image = Utils.getImage(Res.IMG_PLANT_IDLE)
  private var offsetX: Int = 1

  isToRight = true
  isMoving = true

  var chronoPlant: Thread = new Thread(this)
  chronoPlant start()

  def getOffsetX: Int = offsetX

  def setOffsetX(i: Int): Unit = offsetX = i

  override def move(): Unit = {
    if (isToRight){
      setOffsetX(1)
    } else {
      setOffsetX(-1)
    }
    x_=(x + getOffsetX)
  }

  override def run(): Unit = {
    while (true) {
      if (isAlive) {
        move()
        Thread.sleep(Plant.PAUSE)
      }
    }
  }

  def deadImage: Image = Utils.getImage(Res.IMG_PLANT_DEAD)

  def contact(obj: GameObjectImpl): Unit = {
    if (hitAhead(obj) && isToRight) {
      isToRight = false
      offsetX = -1
    } else if (hitBack(obj) && !isToRight) {
      isToRight = true
      offsetX = 1
    }
  }

  def contact(bc: BasicCharacter): Unit = {
    if (hitAhead(bc) && !isToRight) {
      isToRight = true
      offsetX = 1
    }
  }

  override def walk(name: String, frequency: Int): Image = Utils.getImage(Res.IMG_PLANT_IDLE)

}
