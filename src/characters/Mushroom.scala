package characters

import java.awt.Image

import objects.GameObjectImpl
import utils.{Res, Utils}

/**
  * Created by Sofia Rosetti on 11/04/2017.
  */

object Mushroom {
  private val WIDTH = 27
  private val HEIGHT = 30
  private val PAUSE = 15
}
class Mushroom (_x: Int, _y: Int) extends BasicCharacter(_x, _y, Mushroom.WIDTH, Mushroom.HEIGHT) with Runnable {

  private var mushroomImage: Image = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT)
  private var offsetX: Int = 1

  isToRight = true
  isMoving = true

  var chronoMushroom: Thread = new Thread(this)
  chronoMushroom start()

  def getOffsetX: Int = offsetX

  def setOffsetX(i: Int): Unit = offsetX = i

  override def move(): Unit = {
    if (isToRight){
      setOffsetX(1)
    } else {
      setOffsetX(-1)
    }
    x_= (x + getOffsetX)
  }

  override def run(): Unit = {
    while (true) {
      if (isAlive) {
        move()
        Thread.sleep(Mushroom.PAUSE)
      }
    }
  }

  def deadImage: Image = {
    if (isToRight) {
      Utils.getImage(Res.IMG_MUSHROOM_DEAD_DX)
    } else {
      Utils.getImage(Res.IMG_MUSHROOM_DEAD_SX)
    }
  }

  def contact(obj: GameObjectImpl): Unit = {
    if (hitAhead(obj) && isToRight) {
      isToRight = false
      offsetX = -1
    } else if (hitBack(obj) && !isToRight) {
      isToRight = true
      offsetX = 1
    }
  }

  def contact(bc: BasicCharacter): Unit = {
    if (hitAhead(bc) && isToRight) {
      isToRight = false
      offsetX = -1
    } else if (hitBack(bc) && !isToRight) {
      isToRight = true
      offsetX = 1
    }
  }

}
