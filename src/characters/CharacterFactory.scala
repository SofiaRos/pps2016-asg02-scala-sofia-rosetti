package characters

/**
  * Created by Sofia Rosetti on 12/04/2017.
  */
trait CharacterFactory {

  def createMario(x: Int, y: Int): Character

  def createMushroom(x: Int, y: Int): Character

  def createTurtle(x: Int, y: Int): Character

  def createPlant(x: Int, y: Int): Character
}

class CharacterFactoryImpl extends CharacterFactory {

  override def createMario(x: Int, y: Int) = new Mario(x, y)

  override def createMushroom(x: Int, y: Int) = new Mushroom(x, y)

  override def createTurtle(x: Int, y: Int) = new Turtle(x, y)

  override def createPlant(x: Int, y: Int) = new Plant(x, y)

}