package interfaces;

import characters.Mario;
import characters.Plant;
import characters.Turtle;
import characters.Mushroom;

/**
 * Created by Sofia Rosetti on 16/03/2017.
 */
public interface Platform {

    Mario getMario();

    Mushroom getMushroom();

    Turtle getTurtle();

    Plant getPlant();

    int getFloorOffsetY();

    int getHeightLimit();

    int getMov();

    int getXPos();

    void setBackground2PosX(int background2PosX);

    void setFloorOffsetY(int floorOffsetY);

    void setHeightLimit(int heightLimit);

    void setXPos(int xPos);

    void setMov(int mov);

    void setBackground1PosX(int x);

    void updateBackgroundOnMovement();
}
