package game

/**
  * Created by Sofia Rosetti on 12/04/2017.
  */

object Score {

  private var score: Int = 0

  def currentScore: Int = score

  def incrementScore(): Unit = score = score + 1
}
