package objects

import java.awt.Image

import utils.{Res, Utils}

object Piece {
  private val WIDTH: Int = 30
  private val HEIGHT: Int = 30
  private val PAUSE: Int = 10
  private val FLIP_FREQUENCY: Int = 100
}

class Piece(_x: Int, _y: Int) extends GameObjectImpl(_x, _y, Piece.WIDTH, objects.Piece.HEIGHT) with Runnable {
  super.imgObj_=(Utils.getImage(Res.IMG_PIECE1))
  private var counter: Int = 0

  def imageOnMovement: Image = {
    return Utils.getImage(if ( {
      this.counter += 1; this.counter
    } % objects.Piece.FLIP_FREQUENCY == 0) Res.IMG_PIECE1
    else Res.IMG_PIECE2)
  }

  def run() {
    while (true) {
      {
        this.imageOnMovement
        try {
          Thread.sleep(objects.Piece.PAUSE)
        }
        catch {
          case e: InterruptedException => {
          }
        }
      }
    }
  }
}