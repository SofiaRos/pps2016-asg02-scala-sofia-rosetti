package objects

import utils.{Res, Utils}

object Tunnel {
  private val WIDTH: Int = 43
  private val HEIGHT: Int = 65
}

class Tunnel(_x: Int, _y: Int) extends GameObjectImpl(_x, _y, Tunnel.WIDTH, objects.Tunnel.HEIGHT) {
  super.imgObj_=(Utils.getImage(Res.IMG_TUNNEL))
}