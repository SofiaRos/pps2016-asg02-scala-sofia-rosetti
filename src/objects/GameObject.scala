package objects

import java.awt._

import game.PlatformImpl

/**
  * Created by Sofia Rosetti on 09/03/2017.
  */
trait GameObject {

  def width: Int

  def height: Int

  def x: Int

  def y: Int

  def imgObj: Image

  def imgObj_=(imageIn: Image): Unit

  def width_=(width: Int): Unit

  def height_=(height: Int): Unit

  def x_=(x: Int): Unit

  def y_=(y: Int): Unit

  def move(): Unit
}

class GameObjectImpl(var _x: Int, var _y: Int, var _w: Int, var _h: Int) extends GameObject {
  protected var image: Image = null

  override def width: Int = _w

  override def height: Int = _h

  override def x: Int = _x

  override def y: Int = _y

  override def imgObj: Image = image

  override def imgObj_=(imageIn: Image) : Unit = image = imageIn

  override def width_=(width: Int): Unit = _w = width

  override def height_=(height: Int): Unit = _h = height

  override def x_=(x: Int): Unit = _x = x

  override def y_=(y: Int): Unit = _y = y

  override def move(): Unit = PlatformImpl.getPlatform.getXPos match {
    case x if x >= 0 => _x = _x - PlatformImpl.getPlatform.getMov
  }
}