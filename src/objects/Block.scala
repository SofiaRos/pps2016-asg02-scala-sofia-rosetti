package objects

import utils.{Res, Utils}

object Block {
  private val WIDTH: Int = 30
  private val HEIGHT: Int = 30
}

class Block(_x: Int, _y: Int)
  extends GameObjectImpl(_x, _y, Block.WIDTH, objects.Block.HEIGHT) {
  super.imgObj_=(Utils.getImage(Res.IMG_BLOCK))
}