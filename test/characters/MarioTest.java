package characters;

import objects.GameObjectImpl;
import objects.Piece;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Sofia Rosetti on 16/03/2017.
 */
public class MarioTest {

    GameObjectImpl goi;
    BasicCharacter turtle;
    Mario mario;
    Piece piece;

    @Before
    public void initialize() {
        this.goi = new GameObjectImpl(15,0,10,10);
        this.turtle = new Turtle(10,0);
        this.mario = new Mario(25,0);
        this.piece = new Piece(50,0);
    }

    @Test
    public void contactPieceTest() throws Exception {
        assertTrue(mario.contactPiece(piece));
    }

    @Test
    public void contactTestGoi() throws Exception {
        mario.contact(goi);
        assertFalse(mario.isMoving());
    }

    @Test
    public void contactTestBc() throws Exception {
        mario.contact(turtle);
        assertFalse(mario.isMoving());
    }

}